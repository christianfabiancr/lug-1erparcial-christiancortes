USE [master]
GO
/****** Object:  Database [LUG_1erParcial]    Script Date: 29/9/2020 22:15:06 ******/
CREATE DATABASE [LUG_1erParcial]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'LUG_1erParcial', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\LUG_1erParcial.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'LUG_1erParcial_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\LUG_1erParcial_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [LUG_1erParcial] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [LUG_1erParcial].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [LUG_1erParcial] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [LUG_1erParcial] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [LUG_1erParcial] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [LUG_1erParcial] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [LUG_1erParcial] SET ARITHABORT OFF 
GO
ALTER DATABASE [LUG_1erParcial] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [LUG_1erParcial] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [LUG_1erParcial] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [LUG_1erParcial] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [LUG_1erParcial] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [LUG_1erParcial] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [LUG_1erParcial] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [LUG_1erParcial] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [LUG_1erParcial] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [LUG_1erParcial] SET  DISABLE_BROKER 
GO
ALTER DATABASE [LUG_1erParcial] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [LUG_1erParcial] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [LUG_1erParcial] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [LUG_1erParcial] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [LUG_1erParcial] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [LUG_1erParcial] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [LUG_1erParcial] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [LUG_1erParcial] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [LUG_1erParcial] SET  MULTI_USER 
GO
ALTER DATABASE [LUG_1erParcial] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [LUG_1erParcial] SET DB_CHAINING OFF 
GO
ALTER DATABASE [LUG_1erParcial] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [LUG_1erParcial] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [LUG_1erParcial] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [LUG_1erParcial] SET QUERY_STORE = OFF
GO
USE [LUG_1erParcial]
GO
/****** Object:  Table [dbo].[CONCEPTO]    Script Date: 29/9/2020 22:15:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CONCEPTO](
	[nro_concepto] [int] NOT NULL,
	[descripcion] [varchar](50) NOT NULL,
	[porcentaje] [float] NOT NULL,
	[activo] [bit] NOT NULL,
 CONSTRAINT [PK_CONCEPTO] PRIMARY KEY CLUSTERED 
(
	[nro_concepto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EMPLEADO]    Script Date: 29/9/2020 22:15:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EMPLEADO](
	[legajo] [varchar](50) NOT NULL,
	[nombre_apellido] [varchar](50) NOT NULL,
	[cuil] [bigint] NOT NULL,
	[fecha_alta] [datetime] NOT NULL,
	[activo] [bit] NOT NULL,
 CONSTRAINT [PK_EMPLEADO] PRIMARY KEY CLUSTERED 
(
	[legajo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RECIBO]    Script Date: 29/9/2020 22:15:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RECIBO](
	[nro_recibo] [int] NOT NULL,
	[mes] [int] NOT NULL,
	[año] [int] NOT NULL,
	[sueldo_bruto] [float] NOT NULL,
	[sueldo_neto] [float] NOT NULL,
	[total_descuentos] [float] NOT NULL,
	[empleado] [varchar](50) NOT NULL,
	[activo] [bit] NOT NULL,
 CONSTRAINT [PK_RECIBO] PRIMARY KEY CLUSTERED 
(
	[nro_recibo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RECIBO_CONCEPTO]    Script Date: 29/9/2020 22:15:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RECIBO_CONCEPTO](
	[nro_concepto] [int] NOT NULL,
	[nro_recibo] [int] NOT NULL,
 CONSTRAINT [PK_RECIBO_CONCEPTO] PRIMARY KEY CLUSTERED 
(
	[nro_concepto] ASC,
	[nro_recibo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RECIBO]  WITH CHECK ADD  CONSTRAINT [FK_RECIBO_EMPLEADO] FOREIGN KEY([empleado])
REFERENCES [dbo].[EMPLEADO] ([legajo])
GO
ALTER TABLE [dbo].[RECIBO] CHECK CONSTRAINT [FK_RECIBO_EMPLEADO]
GO
ALTER TABLE [dbo].[RECIBO_CONCEPTO]  WITH CHECK ADD  CONSTRAINT [FK_RECIBO_CONCEPTO_CONCEPTO] FOREIGN KEY([nro_concepto])
REFERENCES [dbo].[CONCEPTO] ([nro_concepto])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[RECIBO_CONCEPTO] CHECK CONSTRAINT [FK_RECIBO_CONCEPTO_CONCEPTO]
GO
ALTER TABLE [dbo].[RECIBO_CONCEPTO]  WITH CHECK ADD  CONSTRAINT [FK_RECIBO_CONCEPTO_RECIBO] FOREIGN KEY([nro_recibo])
REFERENCES [dbo].[RECIBO] ([nro_recibo])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[RECIBO_CONCEPTO] CHECK CONSTRAINT [FK_RECIBO_CONCEPTO_RECIBO]
GO
/****** Object:  StoredProcedure [dbo].[DESHABILITAR_CONCEPTO]    Script Date: 29/9/2020 22:15:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[DESHABILITAR_CONCEPTO]
@nro_concepto int
AS
BEGIN
	UPDATE CONCEPTO
	SET ACTIVO = 0
	WHERE nro_concepto = @nro_concepto
END
GO
/****** Object:  StoredProcedure [dbo].[DESHABILITAR_EMPLEADO]    Script Date: 29/9/2020 22:15:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[DESHABILITAR_EMPLEADO]
@legajo varchar(50)
AS
BEGIN
	UPDATE EMPLEADO
	SET ACTIVO = 0
	WHERE legajo = @legajo
END
GO
/****** Object:  StoredProcedure [dbo].[DESHABILITAR_RECIBO]    Script Date: 29/9/2020 22:15:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[DESHABILITAR_RECIBO]
@nro_recibo int
AS
BEGIN
	UPDATE RECIBO
	SET 
		activo = 0
	WHERE 
		nro_recibo = @nro_recibo
END
GO
/****** Object:  StoredProcedure [dbo].[INSERTAR_CONCEPTO]    Script Date: 29/9/2020 22:15:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[INSERTAR_CONCEPTO]
@descripcion varchar(50),@porcentaje float
AS 
BEGIN
	DECLARE @nro_concepto int
	SET @nro_concepto  = ISNULL( (SELECT MAX(nro_concepto) from CONCEPTO) ,0) +1

	INSERT INTO CONCEPTO (nro_concepto,descripcion,porcentaje,activo)
	VALUES (@nro_concepto,@descripcion,@porcentaje,1)
END
GO
/****** Object:  StoredProcedure [dbo].[INSERTAR_EMPLEADO]    Script Date: 29/9/2020 22:15:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[INSERTAR_EMPLEADO]
@legajo varchar(50), @nombre_apellido varchar(50), @cuil bigint, @fecha_alta datetime
AS
BEGIN	
	INSERT INTO EMPLEADO (legajo,nombre_apellido,cuil,fecha_alta,activo) VALUES (@legajo,@nombre_apellido,@cuil,@fecha_alta,1)
END
GO
/****** Object:  StoredProcedure [dbo].[INSERTAR_RECIBO]    Script Date: 29/9/2020 22:15:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[INSERTAR_RECIBO]
@mes int, @año int, @sueldo_bruto float, @sueldo_neto float, @total_descuentos float, @empleado varchar(50)
AS 
BEGIN

	DECLARE @nro_recibo int

	SET @nro_recibo  = ISNULL( (SELECT MAX(nro_recibo) from RECIBO) ,0) +1

	INSERT INTO RECIBO (nro_recibo,mes,año,sueldo_bruto,sueldo_neto,total_descuentos,empleado,activo)
	VALUES (@nro_recibo,@mes,@año,@sueldo_bruto,@sueldo_neto,@total_descuentos,@empleado,1)
END
GO
/****** Object:  StoredProcedure [dbo].[INSERTAR_RECIBO_CONCEPTO]    Script Date: 29/9/2020 22:15:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[INSERTAR_RECIBO_CONCEPTO]
@nro_concepto int, @nro_recibo int
AS 
BEGIN
	INSERT INTO RECIBO_CONCEPTO(nro_concepto,nro_recibo)
	VALUES (@nro_concepto,@nro_recibo)
END
GO
/****** Object:  StoredProcedure [dbo].[LISTAR_CONCEPTO]    Script Date: 29/9/2020 22:15:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[LISTAR_CONCEPTO]
AS 
BEGIN
	SELECT * FROM CONCEPTO
	WHERE ACTIVO = 1
END
GO
/****** Object:  StoredProcedure [dbo].[LISTAR_CONCEPTOS_X_RECIBO]    Script Date: 29/9/2020 22:15:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[LISTAR_CONCEPTOS_X_RECIBO]
@nro_recibo int
AS
BEGIN
	SELECT concepto.nro_concepto,concepto.descripcion,concepto.porcentaje
	FROM CONCEPTO
	INNER JOIN RECIBO_CONCEPTO 
	ON RECIBO_CONCEPTO.nro_concepto = CONCEPTO.nro_concepto
	WHERE RECIBO_CONCEPTO.nro_recibo = @nro_recibo
END
GO
/****** Object:  StoredProcedure [dbo].[LISTAR_EMPLEADO]    Script Date: 29/9/2020 22:15:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[LISTAR_EMPLEADO]
AS 
BEGIN
	SELECT * FROM EMPLEADO
	WHERE ACTIVO = 1
END
GO
/****** Object:  StoredProcedure [dbo].[LISTAR_RECIBO]    Script Date: 29/9/2020 22:15:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[LISTAR_RECIBO]
AS 
BEGIN
	SELECT * FROM RECIBO
	WHERE activo = 1
END
GO
/****** Object:  StoredProcedure [dbo].[LISTAR_RECIBO_INDIVIDUAL]    Script Date: 29/9/2020 22:15:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[LISTAR_RECIBO_INDIVIDUAL]
@legajo varchar(50), @mes int, @año int
AS 
BEGIN
	SELECT * 
	FROM RECIBO
	WHERE empleado = @legajo
	AND mes = @mes
	AND año = @año		
END
GO
/****** Object:  StoredProcedure [dbo].[LISTAR_RECIBOS_EMPLEADO]    Script Date: 29/9/2020 22:15:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[LISTAR_RECIBOS_EMPLEADO]
@legajo varchar(50)
AS 
BEGIN
	SELECT * 
	FROM RECIBO
	WHERE empleado = @legajo
END
GO
/****** Object:  StoredProcedure [dbo].[MODIFICAR_CONCEPTO]    Script Date: 29/9/2020 22:15:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[MODIFICAR_CONCEPTO]
@nro_concepto int, @descripcion varchar(50),@porcentaje float
AS 
BEGIN
	UPDATE CONCEPTO
	SET 
		descripcion = @descripcion,
		porcentaje = @porcentaje
	WHERE 
		nro_concepto = @nro_concepto
END
GO
/****** Object:  StoredProcedure [dbo].[MODIFICAR_EMPLEADO]    Script Date: 29/9/2020 22:15:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[MODIFICAR_EMPLEADO]
@legajo varchar(50), @nombre_apellido varchar(50), @cuil bigint
AS
BEGIN	
	UPDATE EMPLEADO
	SET
		nombre_apellido = @nombre_apellido,
		cuil = @cuil
	WHERE
		legajo = @legajo
END
GO
/****** Object:  StoredProcedure [dbo].[OBTENER_ULTIMO_RECIBO]    Script Date: 29/9/2020 22:15:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[OBTENER_ULTIMO_RECIBO]
AS
BEGIN
	SELECT MAX(nro_recibo) FROM RECIBO
END
GO
USE [master]
GO
ALTER DATABASE [LUG_1erParcial] SET  READ_WRITE 
GO
