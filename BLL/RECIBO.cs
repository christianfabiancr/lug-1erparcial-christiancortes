﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LUG_1erParcial_ACCESO;
using System.Data;
using System.Data.SqlClient;
using System.Security.Cryptography;

namespace BLL
{
    public class RECIBO
    {
		private int nroRecibo;

		public int NroRecibo
		{
			get { return nroRecibo; }
			set { nroRecibo = value; }
		}

		private int mes;

		public int Mes
		{
			get { return mes; }
			set { mes = value; }
		}

		private int año;

		public int Año
		{
			get { return año; }
			set { año = value; }
		}

		private float sueldoBruto;

		public float SueldoBruto
		{
			get { return sueldoBruto; }
			set { sueldoBruto = value; }
		}

		private float sueldoNeto;

		public float SueldoNeto
		{
			get { return sueldoNeto; }
			set { sueldoNeto = value; }
		}

		private float totalDescuentos;

		public float TotalDescuentos
		{
			get { return totalDescuentos; }
			set { totalDescuentos = value; }
		}

		private string empleado;

		public string Empleado
		{
			get { return empleado; }
			set { empleado = value; }
		}

		private bool activo;

		public bool Activo
		{
			get { return activo; }
			set { activo = value; }
		}

		private List<CONCEPTO> conceptos;

		public List<CONCEPTO> Conceptos
		{
			get { return conceptos; }
			set { conceptos = value; }
		}


		public static List<RECIBO> Listar()
		{
			List<RECIBO> lista = new List<RECIBO>();

			ACCESO acceso = new ACCESO();
			acceso.Abrir();
			DataTable tabla = acceso.Leer("LISTAR_RECIBO");
			acceso.Cerrar();

			foreach (DataRow fila in tabla.Rows)
			{
				RECIBO recibo = new RECIBO();
				recibo.nroRecibo = int.Parse(fila[0].ToString());
				recibo.mes = int.Parse(fila[1].ToString());
				recibo.año = int.Parse(fila[2].ToString());
				recibo.sueldoBruto = float.Parse(fila[3].ToString());
				recibo.sueldoNeto = float.Parse(fila[4].ToString());
				recibo.totalDescuentos = float.Parse(fila[5].ToString());
				recibo.empleado = fila[6].ToString();
				recibo.activo = bool.Parse(fila[7].ToString());
				recibo.conceptos = CONCEPTO.ListarConceptosXRecibo(recibo.nroRecibo);

				lista.Add(recibo);
			}

			return lista;
		}

		public static List<RECIBO> ListarRecibosXEmpleado(string legajo)
		{
			List<RECIBO> recibos = new List<RECIBO>();

			ACCESO acceso = new ACCESO();
			acceso.Abrir();
			List<IDbDataParameter> parametros = new List<IDbDataParameter>();
			parametros.Add(acceso.CrearParametro("@legajo", legajo));
			DataTable tabla = acceso.Leer("LISTAR_RECIBOS_EMPLEADO",parametros);
			acceso.Cerrar();

			foreach (DataRow fila in tabla.Rows)
			{
				RECIBO recibo = new RECIBO();
				recibo.nroRecibo = int.Parse(fila[0].ToString());
				recibo.mes = int.Parse(fila[1].ToString());
				recibo.año = int.Parse(fila[2].ToString());
				recibo.sueldoBruto = float.Parse(fila[3].ToString());
				recibo.sueldoNeto = float.Parse(fila[4].ToString());
				recibo.totalDescuentos = float.Parse(fila[5].ToString());
				recibo.empleado = fila[6].ToString();
				recibo.activo = bool.Parse(fila[7].ToString());
				recibo.conceptos = CONCEPTO.ListarConceptosXRecibo(recibo.nroRecibo);

				recibos.Add(recibo);
			}

			return recibos;
		}

		public static int Insertar(int mes, int año, float sueldoBruto, EMPLEADO empleado,List<CONCEPTO> conceptos)
		{
			List<RECIBO> recibos = RECIBO.Listar();

			List<RECIBO> validacion = (from r in recibos
									   where r.activo
									   && r.mes == mes
									   && r.año == año
									   && r.empleado == empleado.Legajo
									   select r).ToList();
			if (validacion.Count == 0)
			{
				if (mes >= 1 && mes <= 12 && año >= DateTime.Now.Year && año <= DateTime.Now.Year + 1)
				{
					if (sueldoBruto > 0)
					{
						ACCESO acceso = new ACCESO();
						acceso.Abrir();
						List<IDbDataParameter> parametros = new List<IDbDataParameter>();

						float sueldoNeto = sueldoBruto;

						foreach (CONCEPTO concepto in conceptos)
						{
							sueldoNeto += sueldoBruto * concepto.Porcentaje;
						}

						parametros.Add(acceso.CrearParametro("@mes", mes));
						parametros.Add(acceso.CrearParametro("@año", año));
						parametros.Add(acceso.CrearParametro("@sueldo_bruto", sueldoBruto));
						parametros.Add(acceso.CrearParametro("@sueldo_neto", sueldoNeto));
						parametros.Add(acceso.CrearParametro("@total_descuentos", sueldoBruto - sueldoNeto));
						parametros.Add(acceso.CrearParametro("@empleado", empleado.Legajo));

						int recibo = acceso.Escribir("INSERTAR_RECIBO", parametros);
						int recibo_concepto = 0;

						DataTable tabla = acceso.Leer("OBTENER_ULTIMO_RECIBO");

						int nroRecibo = int.Parse(((DataRow)tabla.Rows[0])[0].ToString());

						foreach (CONCEPTO concepto in conceptos)
						{
							parametros.Clear();

							parametros.Add(acceso.CrearParametro("@nro_concepto", concepto.NroConcepto));
							parametros.Add(acceso.CrearParametro("@nro_recibo", nroRecibo));

							recibo_concepto += acceso.Escribir("INSERTAR_RECIBO_CONCEPTO", parametros);
						}

						acceso.Cerrar();

						return recibo + recibo_concepto;
					}
					else
					{
						return -7;
					}
				}
				else
				{
					return -6;
				}
			}
			else
			{
				return -5;
			}
		}

		public static int Borrar(int nroRecibo)
		{
			int resultado = 0;
			ACCESO acceso = new ACCESO();
			acceso.Abrir();
			List<IDbDataParameter> parametros = new List<IDbDataParameter>();

			parametros.Add(acceso.CrearParametro("@nro_recibo", nroRecibo));

			resultado = acceso.Escribir("DESHABILITAR_RECIBO", parametros);

			return resultado;
		}

		public override string ToString()
		{
			return nroRecibo + " - " + empleado + " - " + mes + "/" + año;
		}
	}
}