﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using LUG_1erParcial_ACCESO;

namespace BLL
{
    public class CONCEPTO
    {
		private int nroConcepto;

		public int NroConcepto
		{
			get { return nroConcepto; }
			set { nroConcepto = value; }
		}


		private string descripcion;

		public string Descripcion
		{
			get { return descripcion; }
			set { descripcion = value; }
		}

		private float porcentaje;

		public float Porcentaje
		{
			get { return porcentaje; }
			set { porcentaje = value; }
		}

		public static List<CONCEPTO> Listar()
		{
			List<CONCEPTO> lista = new List<CONCEPTO>();

			ACCESO acceso = new ACCESO();
			acceso.Abrir();
			DataTable tabla = acceso.Leer("LISTAR_CONCEPTO");
			acceso.Cerrar();

			foreach (DataRow fila in tabla.Rows)
			{
				CONCEPTO concepto = new CONCEPTO();
				concepto.nroConcepto = int.Parse(fila[0].ToString());
				concepto.descripcion = fila[1].ToString();
				concepto.porcentaje = float.Parse(fila[2].ToString());
				lista.Add(concepto);
			}

			return lista;
		}

		public static int Insertar(string descripcion, float porcentaje)
		{
			ACCESO acceso = new ACCESO();
			acceso.Abrir();
			List<IDbDataParameter> parametros = new List<IDbDataParameter>();

			parametros.Add(acceso.CrearParametro("@descripcion", descripcion));
			parametros.Add(acceso.CrearParametro("@porcentaje", porcentaje)); 
			//A la funcion anterior se le paso un Double como DbType. Validar si funciona.

			int resultado = acceso.Escribir("INSERTAR_CONCEPTO", parametros);

			acceso.Cerrar();

			return resultado;
		}

		public static int Modificar(int nro_concepto, string descripcion, float porcentaje)
		{
			ACCESO acceso = new ACCESO();
			acceso.Abrir();
			List<IDbDataParameter> parametros = new List<IDbDataParameter>();

			parametros.Add(acceso.CrearParametro("@nro_concepto", nro_concepto));
			parametros.Add(acceso.CrearParametro("@descripcion", descripcion));
			parametros.Add(acceso.CrearParametro("@porcentaje", porcentaje));

			int resultado = acceso.Escribir("MODIFICAR_CONCEPTO", parametros);

			acceso.Cerrar();

			return resultado;
		}

		public static int Borrar(int nroConcepto)
		{
			ACCESO acceso = new ACCESO();
			acceso.Abrir();
			List<IDbDataParameter> parametros = new List<IDbDataParameter>();

			parametros.Add(acceso.CrearParametro("@nro_concepto", nroConcepto));

			int resultado = acceso.Escribir("DESHABILITAR_CONCEPTO", parametros);

			acceso.Cerrar();

			return resultado;
		}

		public static List<CONCEPTO> ListarConceptosXRecibo(int nroRecibo)
		{
			List<CONCEPTO> lista = new List<CONCEPTO>();
			
			ACCESO acceso = new ACCESO();
			acceso.Abrir();
			List<IDbDataParameter> parametros = new List<IDbDataParameter>();
			parametros.Add(acceso.CrearParametro("@nro_recibo", nroRecibo));
			
			DataTable tabla = acceso.Leer("LISTAR_CONCEPTOS_X_RECIBO",parametros);
			acceso.Cerrar();

			foreach (DataRow fila in tabla.Rows)
			{
				CONCEPTO concepto = new CONCEPTO();
				concepto.nroConcepto = int.Parse(fila[0].ToString());
				concepto.descripcion = fila[1].ToString();
				concepto.porcentaje = float.Parse(fila[2].ToString());
				lista.Add(concepto);
			}

			return lista;
		}

		public override string ToString()
		{
			return descripcion + " - " + porcentaje*100 + "%";
		}
	}
}