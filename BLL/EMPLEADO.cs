﻿using LUG_1erParcial_ACCESO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;

namespace BLL
{
    public class EMPLEADO
    {
		private string legajo;

		public string Legajo
		{
			get { return legajo; }
			set { legajo = value; }
		}

		private string nombreApellido;

		public string NombreApellido
		{
			get { return nombreApellido; }
			set { nombreApellido = value; }
		}

		private long cuil;

		public long Cuil
		{
			get { return cuil; }
			set { cuil = value; }
		}

		private DateTime fechaAlta;

		public DateTime FechaAlta
		{
			get { return fechaAlta; }
			set { fechaAlta = value; }
		}

		private List<RECIBO> recibos;

		public List<RECIBO> Recibos
		{
			get { return recibos; }
			set { recibos = value; }
		}

		public static List<EMPLEADO> Listar()
		{
			List<EMPLEADO> lista = new List<EMPLEADO>();

			ACCESO acceso = new ACCESO();
			acceso.Abrir();
			DataTable tabla = acceso.Leer("LISTAR_EMPLEADO");
			acceso.Cerrar();

			foreach(DataRow fila in tabla.Rows)
			{
				EMPLEADO empleado = new EMPLEADO();
				empleado.legajo = fila[0].ToString();
				empleado.nombreApellido = fila[1].ToString();
				empleado.cuil = long.Parse(fila[2].ToString());
				empleado.fechaAlta = DateTime.Parse(fila[3].ToString());
				empleado.recibos = RECIBO.ListarRecibosXEmpleado(empleado.legajo);

				lista.Add(empleado);
			}

			return lista;
		}

		public static int Insertar(string legajo, string nombreApellido,long cuil)
		{
			ACCESO acceso = new ACCESO();
			acceso.Abrir();
			List<IDbDataParameter> parametros = new List<IDbDataParameter>();

			parametros.Add(acceso.CrearParametro("@legajo",legajo));
			parametros.Add(acceso.CrearParametro("@nombre_apellido", nombreApellido));
			parametros.Add(acceso.CrearParametro("@cuil", cuil));
			parametros.Add(acceso.CrearParametro("@fecha_alta", DateTime.Now));

			int resultado = acceso.Escribir("INSERTAR_EMPLEADO", parametros);

			acceso.Cerrar();

			return resultado;
		}

		public static int Modificar(string legajo, string nombreApellido, long cuil)
		{
			ACCESO acceso = new ACCESO();
			acceso.Abrir();
			List<IDbDataParameter> parametros = new List<IDbDataParameter>();

			parametros.Add(acceso.CrearParametro("@legajo", legajo));
			parametros.Add(acceso.CrearParametro("@nombre_apellido", nombreApellido));
			parametros.Add(acceso.CrearParametro("@cuil", cuil));

			int resultado = acceso.Escribir("MODIFICAR_EMPLEADO", parametros);

			acceso.Cerrar();

			return resultado;
		}

		public static int Borrar(string legajo)
		{
			ACCESO acceso = new ACCESO();
			acceso.Abrir();
			List<IDbDataParameter> parametros = new List<IDbDataParameter>();

			parametros.Add(acceso.CrearParametro("@legajo", legajo));

			int resultado = acceso.Escribir("DESHABILITAR_EMPLEADO", parametros);

			acceso.Cerrar();

			return resultado;
		}

		public override string ToString()
		{
			return legajo + " - " + nombreApellido;
		}
	}
}