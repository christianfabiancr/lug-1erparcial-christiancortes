﻿using BLL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LUG_1erParcial_Forms
{
    public partial class FormDetalleRecibo : Form
    {
        public EMPLEADO empleado;
        
        public FormDetalleRecibo(EMPLEADO empleado)
        {
            InitializeComponent();
            this.empleado = empleado;
            lblEmpleado.Text = "Empleado: " + empleado.NombreApellido;

            List<RECIBO> recibos = (from r in empleado.Recibos
                                    where r.Activo
                                    select r).ToList();

            cmbRecibos.DataSource = null;
            cmbRecibos.DataSource = recibos;

            if(recibos.Count > 0) 
            {
                enlazar((RECIBO)cmbRecibos.SelectedItem);
            }
            else
            {
                MessageBox.Show("Este empleado no tiene ningún recibo.");
            }
        }

        public void enlazar(RECIBO recibo)
        {
            lblMes.Text = "Mes: " + recibo.Mes;
            lblAño.Text = "Año: " + recibo.Año;
            lblSueldoB.Text = "Sueldo bruto: $" + recibo.SueldoBruto;
            lblSueldoN.Text = "Sueldo neto: $" + recibo.SueldoNeto;
            lblDescuentos.Text = "Total descuentos: $" + recibo.TotalDescuentos;
            lstConceptos.DataSource = null;
            lstConceptos.DataSource = recibo.Conceptos;
        }

        private void FormDetalleRecibo_Load(object sender, EventArgs e)
        {

        }

        private void lblRecibo_Click(object sender, EventArgs e)
        {

        }

        private void cmbRecibos_SelectedIndexChanged(object sender, EventArgs e)
        {
            enlazar((RECIBO)cmbRecibos.SelectedItem);
        }
    }
}
