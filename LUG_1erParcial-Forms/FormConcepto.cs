﻿using BLL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LUG_1erParcial_Forms
{
    public partial class FormConcepto : Form
    {

        public bool editando;
        public int nroConcepto;
        public void cargarDatos(int nroConcepto,string descripcion,float porcentaje)
        {
            this.nroConcepto = nroConcepto;
            txtDescripcion.Text = descripcion;
            txtPorcentaje.Text = porcentaje.ToString();
            this.Text = "Editar concepto";
            btnConcepto.Text = "EDITAR";
        }

        public FormConcepto()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(!string.IsNullOrWhiteSpace(txtDescripcion.Text) && !string.IsNullOrWhiteSpace(txtPorcentaje.Text))
            {
                try
                {
                    if (float.Parse(txtPorcentaje.Text) <= 1 && float.Parse(txtPorcentaje.Text) >= -1)
                    {
                        int resultado;

                        if (!editando)
                        {
                            resultado = CONCEPTO.Insertar(txtDescripcion.Text, float.Parse(txtPorcentaje.Text));
                        }
                        else
                        {
                            resultado = CONCEPTO.Modificar(nroConcepto, txtDescripcion.Text, float.Parse(txtPorcentaje.Text));
                        }

                        if (resultado > 0)
                        {
                            if (!editando)
                            {
                                MessageBox.Show("Se ha creado el concepto " + txtDescripcion.Text);
                                txtDescripcion.Clear();
                                txtPorcentaje.Clear();
                            }
                            else
                            {
                                MessageBox.Show("Se ha actualizado el concepto " + txtDescripcion.Text);
                                this.Close();
                            }
                        }
                        else
                        {
                            MessageBox.Show("Error de carga. Revise los datos ingresados.", "ERROR");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Ingrese un valor entre 1 (100%) y -1 (-100%), utilizando una coma para los decimales.", "ERROR");
                    }
                }
                catch
                {
                    MessageBox.Show("Revise los valores ingresados","ERROR");
                }
            }
            else
            {
                MessageBox.Show("Complete todos los campos.", "ERROR");
            }
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void FormConcepto_Load(object sender, EventArgs e)
        {

        }
    }
}
