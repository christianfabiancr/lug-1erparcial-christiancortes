﻿using BLL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LUG_1erParcial_Forms
{
    public partial class FormRecibo : Form
    {
        public FormRecibo()
        {
            InitializeComponent();
        }

        private void FormRecibo_Load(object sender, EventArgs e)
        {
            comboEmpleado.DataSource = null;
            comboEmpleado.DataSource = EMPLEADO.Listar();
            comboConceptos.DataSource = null;
            comboConceptos.DataSource = CONCEPTO.Listar();
        }

        private void btnAñadir_Click(object sender, EventArgs e)
        {
            if(comboConceptos.SelectedItem != null)
            {
                if (!lstConceptos.Items.Contains(comboConceptos.SelectedItem))
                {
                    lstConceptos.Items.Add((CONCEPTO)comboConceptos.SelectedItem);
                }
                else
                {
                    MessageBox.Show("Ya ha seleccionado este concepto.","ERROR");
                }
            }
            else
            {
                MessageBox.Show("Seleccione un concepto.", "ERROR");
            }
        }

        private void btnQuitar_Click(object sender, EventArgs e)
        {
            if(lstConceptos.SelectedItem != null)
            {
                lstConceptos.Items.Remove(lstConceptos.SelectedItem);
            }
            else
            {
                MessageBox.Show("Seleccione un concepto.", "ERROR");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(!string.IsNullOrWhiteSpace(txtSueldo.Text) && !string.IsNullOrWhiteSpace(txtMes.Text) &&
               !string.IsNullOrWhiteSpace(txtAño.Text) && !string.IsNullOrWhiteSpace(comboEmpleado.Text))
            {
                int resultado = RECIBO.Insertar(int.Parse(txtMes.Text), int.Parse(txtAño.Text), float.Parse(txtSueldo.Text),
                    (EMPLEADO)comboEmpleado.SelectedItem, lstConceptos.Items.Cast<CONCEPTO>().ToList());
                if(resultado > 0)
                {
                    MessageBox.Show("Recibo creado.");
                }
                else if(resultado == -5)
                {
                    MessageBox.Show("Ya existe un recibo para el empleado en el mes y año indicados.");
                }
                else if( resultado == -6)
                {
                    MessageBox.Show("Ingrese un mes entre 1 y 12, y un año entre el actual y el próximo.", "ERROR");
                }
                else if(resultado == -7)
                {
                    MessageBox.Show("El sueldo no puede ser negativo o nulo.");
                }
            }
            else
            {
                MessageBox.Show("Complete todos los campos", "ERROR");
            }
        }

        private void txtAño_TextChanged(object sender, EventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(txtAño.Text, "[^0-9]"))
            {
                MessageBox.Show("Ingrese solo numeros.");
                txtAño.Text = txtAño.Text.Remove(txtAño.Text.Length - 1);
            }
        }

        private void txtMes_TextChanged(object sender, EventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(txtMes.Text, "[^0-9]"))
            {
                MessageBox.Show("Ingrese solo numeros.");
                txtMes.Text = txtMes.Text.Remove(txtMes.Text.Length - 1);
            }
        }

        private void txtSueldo_TextChanged(object sender, EventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(txtSueldo.Text, "[^0-9]"))
            {
                MessageBox.Show("Ingrese solo numeros.");
                txtSueldo.Text = txtSueldo.Text.Remove(txtSueldo.Text.Length - 1);
            }
        }
    }
}
