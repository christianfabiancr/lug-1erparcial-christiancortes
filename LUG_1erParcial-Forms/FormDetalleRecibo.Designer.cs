﻿namespace LUG_1erParcial_Forms
{
    partial class FormDetalleRecibo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblEmpleado = new System.Windows.Forms.Label();
            this.lblRecibo = new System.Windows.Forms.Label();
            this.cmbRecibos = new System.Windows.Forms.ComboBox();
            this.lblMes = new System.Windows.Forms.Label();
            this.lblAño = new System.Windows.Forms.Label();
            this.lblSueldoB = new System.Windows.Forms.Label();
            this.lblSueldoN = new System.Windows.Forms.Label();
            this.lblDescuentos = new System.Windows.Forms.Label();
            this.lstConceptos = new System.Windows.Forms.ListBox();
            this.lblConceptos = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblEmpleado
            // 
            this.lblEmpleado.AutoSize = true;
            this.lblEmpleado.Location = new System.Drawing.Point(40, 16);
            this.lblEmpleado.Name = "lblEmpleado";
            this.lblEmpleado.Size = new System.Drawing.Size(57, 13);
            this.lblEmpleado.TabIndex = 0;
            this.lblEmpleado.Text = "Empleado:";
            // 
            // lblRecibo
            // 
            this.lblRecibo.AutoSize = true;
            this.lblRecibo.Location = new System.Drawing.Point(53, 38);
            this.lblRecibo.Name = "lblRecibo";
            this.lblRecibo.Size = new System.Drawing.Size(44, 13);
            this.lblRecibo.TabIndex = 1;
            this.lblRecibo.Text = "Recibo:";
            this.lblRecibo.Click += new System.EventHandler(this.lblRecibo_Click);
            // 
            // cmbRecibos
            // 
            this.cmbRecibos.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbRecibos.FormattingEnabled = true;
            this.cmbRecibos.Location = new System.Drawing.Point(97, 35);
            this.cmbRecibos.Name = "cmbRecibos";
            this.cmbRecibos.Size = new System.Drawing.Size(217, 21);
            this.cmbRecibos.TabIndex = 2;
            this.cmbRecibos.SelectedIndexChanged += new System.EventHandler(this.cmbRecibos_SelectedIndexChanged);
            // 
            // lblMes
            // 
            this.lblMes.AutoSize = true;
            this.lblMes.Location = new System.Drawing.Point(67, 62);
            this.lblMes.Name = "lblMes";
            this.lblMes.Size = new System.Drawing.Size(30, 13);
            this.lblMes.TabIndex = 3;
            this.lblMes.Text = "Mes:";
            // 
            // lblAño
            // 
            this.lblAño.AutoSize = true;
            this.lblAño.Location = new System.Drawing.Point(67, 84);
            this.lblAño.Name = "lblAño";
            this.lblAño.Size = new System.Drawing.Size(29, 13);
            this.lblAño.TabIndex = 4;
            this.lblAño.Text = "Año:";
            // 
            // lblSueldoB
            // 
            this.lblSueldoB.AutoSize = true;
            this.lblSueldoB.Location = new System.Drawing.Point(23, 107);
            this.lblSueldoB.Name = "lblSueldoB";
            this.lblSueldoB.Size = new System.Drawing.Size(73, 13);
            this.lblSueldoB.TabIndex = 5;
            this.lblSueldoB.Text = "Sueldo bruto: ";
            // 
            // lblSueldoN
            // 
            this.lblSueldoN.AutoSize = true;
            this.lblSueldoN.Location = new System.Drawing.Point(26, 129);
            this.lblSueldoN.Name = "lblSueldoN";
            this.lblSueldoN.Size = new System.Drawing.Size(70, 13);
            this.lblSueldoN.TabIndex = 6;
            this.lblSueldoN.Text = "Sueldo neto: ";
            // 
            // lblDescuentos
            // 
            this.lblDescuentos.AutoSize = true;
            this.lblDescuentos.Location = new System.Drawing.Point(1, 151);
            this.lblDescuentos.Name = "lblDescuentos";
            this.lblDescuentos.Size = new System.Drawing.Size(95, 13);
            this.lblDescuentos.TabIndex = 7;
            this.lblDescuentos.Text = "Total descuentos: ";
            // 
            // lstConceptos
            // 
            this.lstConceptos.FormattingEnabled = true;
            this.lstConceptos.Location = new System.Drawing.Point(12, 193);
            this.lstConceptos.Name = "lstConceptos";
            this.lstConceptos.Size = new System.Drawing.Size(301, 95);
            this.lstConceptos.TabIndex = 8;
            // 
            // lblConceptos
            // 
            this.lblConceptos.AutoSize = true;
            this.lblConceptos.Location = new System.Drawing.Point(9, 177);
            this.lblConceptos.Name = "lblConceptos";
            this.lblConceptos.Size = new System.Drawing.Size(58, 13);
            this.lblConceptos.TabIndex = 9;
            this.lblConceptos.Text = "Conceptos";
            // 
            // FormDetalleRecibo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(326, 300);
            this.Controls.Add(this.lblConceptos);
            this.Controls.Add(this.lstConceptos);
            this.Controls.Add(this.lblDescuentos);
            this.Controls.Add(this.lblSueldoN);
            this.Controls.Add(this.lblSueldoB);
            this.Controls.Add(this.lblAño);
            this.Controls.Add(this.lblMes);
            this.Controls.Add(this.cmbRecibos);
            this.Controls.Add(this.lblRecibo);
            this.Controls.Add(this.lblEmpleado);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FormDetalleRecibo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormDetalleRecibo";
            this.Load += new System.EventHandler(this.FormDetalleRecibo_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblEmpleado;
        private System.Windows.Forms.Label lblRecibo;
        private System.Windows.Forms.ComboBox cmbRecibos;
        private System.Windows.Forms.Label lblMes;
        private System.Windows.Forms.Label lblAño;
        private System.Windows.Forms.Label lblSueldoB;
        private System.Windows.Forms.Label lblSueldoN;
        private System.Windows.Forms.Label lblDescuentos;
        private System.Windows.Forms.ListBox lstConceptos;
        private System.Windows.Forms.Label lblConceptos;
    }
}