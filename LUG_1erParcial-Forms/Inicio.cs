﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLL;

namespace LUG_1erParcial_Forms
{
    public partial class Form1 : Form
    {
        FormEmpleado FormularioEmpleado;
        FormConcepto FormularioConcepto;
        FormRecibo FormularioRecibo;
        FormDetalleRecibo DetalleRecibos;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private void crearToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormularioEmpleado = new FormEmpleado();
            FormularioEmpleado.ShowDialog();

            dataGridView.DataSource = null;
        }

        private void crearToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            FormularioConcepto = new FormConcepto();
            FormularioConcepto.ShowDialog();

            dataGridView.DataSource = null;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (radioEmpleados.Checked)
            {
                dataGridView.DataSource = null;
                dataGridView.DataSource = EMPLEADO.Listar();
                groupBox.Text = "Empleados";
            }
            else if (radioConceptos.Checked)
            {
                dataGridView.DataSource = null;
                dataGridView.DataSource = CONCEPTO.Listar();
                groupBox.Text = "Conceptos";
            }
            else if (radioRecibos.Checked)
            {
                dataGridView.DataSource = null;
                dataGridView.DataSource = RECIBO.Listar();
                groupBox.Text = "Recibos";
            }
            else
            {
                MessageBox.Show("Seleccione lo que desee listar.","ERROR");
            }
        }

        private void crearToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            FormularioRecibo = new FormRecibo();
            FormularioRecibo.ShowDialog();
            dataGridView.DataSource = null;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (dataGridView.SelectedRows.Count > 0)
            {
                if (dataGridView.SelectedRows[0].DataBoundItem is EMPLEADO)
                {
                    FormularioEmpleado = new FormEmpleado();
                    FormularioEmpleado.editando = true;
                    FormularioEmpleado.cargarDatos(((EMPLEADO)dataGridView.SelectedRows[0].DataBoundItem).Legajo,
                        ((EMPLEADO)dataGridView.SelectedRows[0].DataBoundItem).NombreApellido,
                        ((EMPLEADO)dataGridView.SelectedRows[0].DataBoundItem).Cuil);
                    FormularioEmpleado.ShowDialog();
                    dataGridView.DataSource = null;
                }
                else if(dataGridView.SelectedRows[0].DataBoundItem is CONCEPTO)
                {
                    FormularioConcepto = new FormConcepto();
                    FormularioConcepto.editando = true;
                    FormularioConcepto.cargarDatos(((CONCEPTO)dataGridView.SelectedRows[0].DataBoundItem).NroConcepto,
                        ((CONCEPTO)dataGridView.SelectedRows[0].DataBoundItem).Descripcion,
                        ((CONCEPTO)dataGridView.SelectedRows[0].DataBoundItem).Porcentaje);
                    FormularioConcepto.ShowDialog();
                    dataGridView.DataSource = null;
                }
                else
                {
                    MessageBox.Show("Los recibos no pueden ser modificados. En caso de haber cometido un error, eliminelo y créelo nuevamente.", "ERROR");
                }
            }
            else
            {
                MessageBox.Show("Seleccione un empleado o concepto a editar.","ERROR");
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (dataGridView.SelectedRows.Count > 0)
            {
                if (dataGridView.SelectedRows[0].DataBoundItem is EMPLEADO)
                {
                    EMPLEADO.Borrar(((EMPLEADO)dataGridView.SelectedRows[0].DataBoundItem).Legajo);
                }
                else if (dataGridView.SelectedRows[0].DataBoundItem is CONCEPTO)
                {
                    CONCEPTO.Borrar(((CONCEPTO)dataGridView.SelectedRows[0].DataBoundItem).NroConcepto);
                }
                else
                {
                    RECIBO.Borrar(((RECIBO)dataGridView.SelectedRows[0].DataBoundItem).NroRecibo);
                }
                dataGridView.DataSource = null;
            }
            else
            {
                MessageBox.Show("Seleccione un empleado, concepto o recibo a eliminar.", "ERROR");
            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            if (dataGridView.SelectedRows.Count > 0)
            {
                if (dataGridView.SelectedRows[0].DataBoundItem is EMPLEADO)
                {
                    DetalleRecibos = new FormDetalleRecibo((EMPLEADO)dataGridView.SelectedRows[0].DataBoundItem);
                    DetalleRecibos.Text = "Recibos de " + ((EMPLEADO)dataGridView.SelectedRows[0].DataBoundItem).NombreApellido;
                    DetalleRecibos.ShowDialog();
                }
                else
                {
                    MessageBox.Show("Seleccione un empleado.", "ERROR");
                }
            }
            else
            {
                MessageBox.Show("Seleccione un empleado.", "ERROR");
            }
        }
    }
}
