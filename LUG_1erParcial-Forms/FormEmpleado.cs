﻿using BLL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LUG_1erParcial_Forms
{
    public partial class FormEmpleado : Form
    {
        public bool editando;

        public void cargarDatos(string legajo,string nombre, long cuil)
        {
            btnEmpleado.Text = "EDITAR";
            this.Text = "Editar Empleado";

            txtLegajo.Text = legajo;
            txtLegajo.Enabled = false;
            txtNombre.Text = nombre;
            txtCUIL.Text = cuil.ToString();
        }
        public FormEmpleado()
        {
            InitializeComponent();
        }

        private void FormEmpleado_Load(object sender, EventArgs e)
        {

        }

        private void btnEmpleado_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(txtLegajo.Text) && !string.IsNullOrWhiteSpace(txtNombre.Text)
                && !string.IsNullOrWhiteSpace(txtCUIL.Text))
            {
                int resultado;

                if (!editando)
                {
                    resultado = EMPLEADO.Insertar(txtLegajo.Text, txtNombre.Text, long.Parse(txtCUIL.Text));
                }
                else
                {
                    resultado = EMPLEADO.Modificar(txtLegajo.Text, txtNombre.Text, long.Parse(txtCUIL.Text));
                }

                if (resultado > 0)
                {
                    if (!editando)
                    {
                        MessageBox.Show("El empleado " + txtLegajo.Text + " se ha creado exitósamente.");
                        txtLegajo.Clear();
                        txtNombre.Clear();
                        txtCUIL.Clear();
                    }
                    else
                    {
                        MessageBox.Show("El empleado " + txtLegajo.Text + " se ha actualizado exitósamente.");
                        this.Close();
                    }
                }
                else if(resultado == -2)
                {
                    MessageBox.Show("Ya existe un empleado con el legajo indicado.", "ERROR");
                    txtLegajo.Clear();
                }
                else
                {
                    if (!editando)
                    {
                        MessageBox.Show("Error al crear el empleado.");
                    }
                    else
                    {
                        MessageBox.Show("Error al actualizar el empleado.");
                    }
                }
            }
            else
            {
                MessageBox.Show("Complete todos los campos.", "ERROR");
            }
        }

        private void txtCUIL_TextChanged(object sender, EventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(txtCUIL.Text, "[^0-9]"))
            {
                MessageBox.Show("Ingrese solo numeros.");
                txtCUIL.Text = txtCUIL.Text.Remove(txtCUIL.Text.Length - 1);
            }
        }
    }
}
