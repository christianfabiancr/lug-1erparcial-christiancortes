﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace LUG_1erParcial_ACCESO
{
    public class ACCESO
    {
        private SqlConnection conexion;
        private SqlTransaction transaccion;

        public bool Abrir()
        {
            bool ok;
            if (conexion != null && conexion.State == System.Data.ConnectionState.Open)
            {
                ok = false;
            }
            else
            {
                conexion = new SqlConnection();

                try
                {
                    conexion.ConnectionString = @"Data Source=LAPTOP-6LAM2SPO; Initial Catalog= LUG_1erParcial; Integrated Security=SSPI";

                    conexion.Open();
                    ok = true;
                }
                catch (SqlException ex)
                {
                    ok = false;
                }

            }
            return ok;
        }

        public void Cerrar()
        {
            conexion.Close();
            conexion = null;
            GC.Collect();
        }

        private SqlCommand CrearComando(string Sql, List<IDbDataParameter> parametros = null, CommandType tipo = CommandType.StoredProcedure)
        {
            SqlCommand comando = new SqlCommand(Sql, conexion);
            comando.CommandType = tipo;
            if (parametros != null && parametros.Count > 0)
            {
                comando.Parameters.AddRange(parametros.ToArray());
            }
            return comando;
        }

        public int Escribir(string Sql, List<IDbDataParameter> parametros = null)
        {
            SqlCommand comando = CrearComando(Sql, parametros);
            int resultado;
            try
            {
                resultado = comando.ExecuteNonQuery();
            }
            catch(SqlException ex)
            {
                if (ex.Number == 2627)
                {
                    resultado = -2;
                }
                else
                {
                    resultado = -1;
                }
            }

            return resultado;
        }

        public DataTable Leer(string Sql, List<IDbDataParameter> parametros = null)
        {
            SqlDataAdapter adaptador = new SqlDataAdapter();
            adaptador.SelectCommand = CrearComando(Sql, parametros);
            DataTable tabla = new DataTable();
            adaptador.Fill(tabla);

            return tabla;
        }

        public IDbDataParameter CrearParametro(string nom, string valor)
        {
            SqlParameter parametro = new SqlParameter(nom, valor);
            parametro.DbType = DbType.String;
            return parametro;
        }

        public IDbDataParameter CrearParametro(string nom, int valor)
        {
            SqlParameter parametro = new SqlParameter(nom, valor);
            parametro.DbType = DbType.Int32;
            return parametro;
        }

        public IDbDataParameter CrearParametro(string nom, float valor)
        {
            SqlParameter parametro = new SqlParameter(nom, valor);
            parametro.DbType = DbType.Double;
            return parametro;
        }

        public IDbDataParameter CrearParametro(string nom, long valor)
        {
            SqlParameter parametro = new SqlParameter(nom, valor);
            parametro.DbType = DbType.Int64;
            return parametro;
        }

        public IDbDataParameter CrearParametro(string nom, DateTime valor)
        {
            SqlParameter parametro = new SqlParameter(nom, valor);
            parametro.DbType = DbType.DateTime;
            return parametro;
        }
    }
}
